import {Module, HttpModule} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {EventGateway} from './event.gateway';
import {AuthModule} from './auth/auth.module';
import {ConfigModule} from '@nestjs/config';
import {SubscriberService} from './middleware/subscriber/subscriber.service';
import {UsersController} from './controllers/users/users.controller';
import {
  RefreshUserService
} from './application/commands/refresh-user/refresh-user.service';
import {GetUserService} from './application/queries/get-user/get-user.service';
import {
  GetUsersService
} from './application/queries/get-users/get-users.service';
import {
  DeleteUserService
} from './application/commands/delete-user/delete-user.service';
import {MessagesController} from './controllers/messages/messages.controller';
import {
  ConversationsController
} from './controllers/conversations/conversations.controller';
import {
  GetConversationsService
} from './application/queries/get-conversations/get-conversations.service';
import {
  LeaveConversationService
} from './application/commands/leave-conversation/leave-conversation.service';
import {
  CreateConversationService
} from './application/commands/create-conversation/create-conversation.service';
import {
  UpdateConversationService
} from './application/commands/update-conversation/update-conversation.service';
import {
  SendMessageService
} from './application/commands/send-message/send-message.service';
import {
  MessagesRemoteProxyService
} from './application/remote-proxies/messages-remote-proxy/messages-remote-proxy.service';
import {
  ConversationsRemoteProxyService
} from './application/remote-proxies/conversations-remote-proxy/conversations-remote-proxy.service';
import {
  UsersRemoteProxyService
} from './application/remote-proxies/users-remote-proxy/users-remote-proxy.service';

@Module({
  imports: [
    AuthModule,
    HttpModule,
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
  ],
  controllers: [
    AppController,
    UsersController,
    MessagesController,
    ConversationsController,
  ],
  providers: [
    AppService,
    EventGateway,
    SubscriberService,
    RefreshUserService,
    GetUserService,
    GetUsersService,
    DeleteUserService,
    GetConversationsService,
    LeaveConversationService,
    CreateConversationService,
    UpdateConversationService,
    SendMessageService,
    MessagesRemoteProxyService,
    ConversationsRemoteProxyService,
    UsersRemoteProxyService,
  ],
})
export class AppModule {
}
