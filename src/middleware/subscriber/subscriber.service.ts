import { Injectable } from '@nestjs/common';
import * as Amqp from 'amqp-ts';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SubscriberService {
  private connection: Amqp.Connection = null;
  private exchange: Amqp.Exchange = null;
  private queues: Array<Amqp.Queue> = [];

  constructor(private readonly configService: ConfigService) {
    const amqpConnectionString = this.configService.get<string>(
      'amqpConnectionString',
    );
    const amqpExchangeName = this.configService.get<string>('amqpExchangeName');
    const amqpExchangeType = this.configService.get<string>('amqpExchangeType');
    this.connectToAmqpServer(
      amqpConnectionString,
      amqpExchangeName,
      amqpExchangeType,
    );
  }

  private connectToAmqpServer(
    amqpConnectionString: string,
    amqpExchangeName: string,
    amqpExchangeType: string,
  ) {
    this.connection = new Amqp.Connection(amqpConnectionString);
    this.exchange = this.connection.declareExchange(
      amqpExchangeName,
      amqpExchangeType,
      {
        durable: true,
        autoDelete: false,
      },
    );
  }

  subscribe(queueName: string, binding: string, callback): void {
    const queue = this.connection.declareQueue(queueName);
    queue.bind(this.exchange, binding);
    queue.activateConsumer(callback, { noAck: false });
    this.queues.push(queue);
  }
}
