import {
  Controller,
  UseGuards,
  Get,
  Put,
  Request,
  Param,
  Post,
  Body
} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {
  GetConversationsService
} from 'src/application/queries/get-conversations/get-conversations.service';
import {
  LeaveConversationService
} from 'src/application/commands/leave-conversation/leave-conversation.service';
import {
  CreateConversationService
} from 'src/application/commands/create-conversation/create-conversation.service';
import {
  UpdateConversationService
} from 'src/application/commands/update-conversation/update-conversation.service';

@Controller('conversations')
export class ConversationsController {
  constructor(
      private readonly getConversationsService: GetConversationsService,
      private readonly leaveConversationService: LeaveConversationService,
      private readonly createConversationService: CreateConversationService,
      private readonly updateConversationService: UpdateConversationService) {}

  @UseGuards(AuthGuard('custom'))
  @Get()
  async getConversations(@Request() request) {
    const userId = request.user.user_id;
    return this.getConversationsService.execute(userId);
  }

  @UseGuards(AuthGuard('custom'))
  @Put(":id/leave")
  async leaveConversation(@Request() request, @Param() params) {
    const userId = request.user.user_id;
    const conversationId = params.id;
    return this.leaveConversationService.execute(conversationId, userId);
  }

  @UseGuards(AuthGuard('custom'))
  @Post()
  async create(@Request() request, @Body() conversation) {
    const userId = request.user.user_id;
    return this.createConversationService.execute(conversation, userId);
  }

  @UseGuards(AuthGuard('custom'))
  @Put()
  async update(@Request() request, @Body() conversation) {
    const userId = request.user.user_id;
    return this.updateConversationService.execute(conversation, userId);
  }
}
