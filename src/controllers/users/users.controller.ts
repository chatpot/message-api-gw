import {
  Controller,
  UseGuards,
  Put,
  Request,
  Param,
  Get,
  UnauthorizedException,
  Delete,
} from '@nestjs/common';
import {
  RefreshUserService
} from 'src/application/commands/refresh-user/refresh-user.service';
import {AuthGuard} from '@nestjs/passport';
import {
  GetUserService
} from 'src/application/queries/get-user/get-user.service';
import {
  GetUsersService
} from 'src/application/queries/get-users/get-users.service';
import {
  DeleteUserService
} from 'src/application/commands/delete-user/delete-user.service';

@Controller('users')
export class UsersController {
  constructor(
      private readonly refreshUserService: RefreshUserService,
      private readonly getUserService: GetUserService,
      private readonly getUsersService: GetUsersService,
      private readonly deleteUserService: DeleteUserService, ) {}

  @UseGuards(AuthGuard('custom'))
  @Put('refresh')
  async getProfile(@Request() req) {
    return this.refreshUserService.execute({
      id: req.user.user_id,
      displayName: req.user.name,
      image: req.user.picture,
    });
  }

  @UseGuards(AuthGuard('custom'))
  @Get(':id')
  async getUser(@Param() params) {
    return this.getUserService.execute(params.id);
  }

  @UseGuards(AuthGuard('custom'))
  @Delete(':id')
  async DeleteUser(@Request() request, @Param() params) {
    if (request.user.user_id != params.id) {
      throw new UnauthorizedException();
    }
    return this.deleteUserService.execute(params.id);
  }

  @UseGuards(AuthGuard('custom'))
  @Get()
  async getUsers() {
    return this.getUsersService.execute();
  }
}
