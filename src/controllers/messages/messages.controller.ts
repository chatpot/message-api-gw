import { Controller, UseGuards, Request, Post, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { SendMessageService } from 'src/application/commands/send-message/send-message.service';

@Controller('messages')
export class MessagesController {
  constructor(private readonly sendMessageService: SendMessageService) {}

  @UseGuards(AuthGuard('custom'))
  @Post()
  async sendMessage(@Request() request, @Body() message) {
    const userId = request.user.user_id;
    return this.sendMessageService.execute(userId, message);
  }
}
