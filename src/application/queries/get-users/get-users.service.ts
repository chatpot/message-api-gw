import {Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';
import {
  UsersRemoteProxyService
} from 'src/application/remote-proxies/users-remote-proxy/users-remote-proxy.service';

@Injectable()
export class GetUsersService {
  constructor(
      private readonly usersRemoteProxyService: UsersRemoteProxyService) {}

  execute(): Observable<Array<any>> {
    return this.usersRemoteProxyService.getUsers();
  }
}
