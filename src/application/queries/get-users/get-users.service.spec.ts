import { Test, TestingModule } from '@nestjs/testing';
import { GetUsersService } from './get-users.service';

describe('GetUsersService', () => {
  let service: GetUsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GetUsersService],
    }).compile();

    service = module.get<GetUsersService>(GetUsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
