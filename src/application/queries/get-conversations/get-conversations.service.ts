import { Injectable } from '@nestjs/common';
import { Observable, forkJoin } from 'rxjs';
import { ConversationsRemoteProxyService } from 'src/application/remote-proxies/conversations-remote-proxy/conversations-remote-proxy.service';
import { map, flatMap } from 'rxjs/operators';
import { MessagesRemoteProxyService } from 'src/application/remote-proxies/messages-remote-proxy/messages-remote-proxy.service';

@Injectable()
export class GetConversationsService {
  constructor(
    private readonly conversationsRemoteProxyService: ConversationsRemoteProxyService,
    private readonly messagesRemoteProxyService: MessagesRemoteProxyService,
  ) {}

  execute(userId: string): Observable<any> {
    return this.conversationsRemoteProxyService
      .getConversationsForUser(userId)
      .pipe(
        flatMap(conversations => {
          return forkJoin(
            conversations.map(conversation => {
              return this.messagesRemoteProxyService
                .getMessagesForConversationId(conversation.id)
                .pipe(
                  map(messages => {
                    return { ...conversation, messages: messages };
                  }),
                );
            }),
          );
        }),
      );
  }
}
