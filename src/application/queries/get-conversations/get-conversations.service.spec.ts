import { Test, TestingModule } from '@nestjs/testing';
import { GetConversationsService } from './get-conversations.service';

describe('GetConversationsService', () => {
  let service: GetConversationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GetConversationsService],
    }).compile();

    service = module.get<GetConversationsService>(GetConversationsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
