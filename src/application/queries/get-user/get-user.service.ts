import {Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';
import {
  UsersRemoteProxyService
} from 'src/application/remote-proxies/users-remote-proxy/users-remote-proxy.service';

@Injectable()
export class GetUserService {
  constructor(
      private readonly usersRemoteProxyService: UsersRemoteProxyService) {}

  execute(id: string): Observable<any> {
    return this.usersRemoteProxyService.getUser(id);
  }
}
