import { Test, TestingModule } from '@nestjs/testing';
import { UpdateConversationService } from './update-conversation.service';

describe('UpdateConversationService', () => {
  let service: UpdateConversationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UpdateConversationService],
    }).compile();

    service = module.get<UpdateConversationService>(UpdateConversationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
