import {Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';
import {
  ConversationsRemoteProxyService
} from 'src/application/remote-proxies/conversations-remote-proxy/conversations-remote-proxy.service';

@Injectable()
export class UpdateConversationService {
  constructor(
      private readonly conversationsRemoteProxyService:
          ConversationsRemoteProxyService) {}

  execute(conversation: any, userId: any): Observable<any> {
    return this.conversationsRemoteProxyService.updateConversation({
      ...conversation,
      members: conversation.members.includes(userId) ?
          [...conversation.members] :
          [...conversation.members, userId]
    });
  }
}
