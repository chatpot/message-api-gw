import { Injectable, BadRequestException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ConversationsRemoteProxyService } from 'src/application/remote-proxies/conversations-remote-proxy/conversations-remote-proxy.service';
import { flatMap, catchError } from 'rxjs/operators';

@Injectable()
export class LeaveConversationService {
  constructor(
    private readonly conversationsRemoteProxyService: ConversationsRemoteProxyService,
  ) {}

  execute(conversationId: string, userId: string): Observable<any> {
    return this.conversationsRemoteProxyService
      .getConversation(conversationId)
      .pipe(
        flatMap(conversation => {
          if (conversation.members.includes(userId)) {
            return this.conversationsRemoteProxyService.updateConversation({
              ...conversation,
              members: conversation.members.filter(member => member != userId),
            });
          }
          throw new BadRequestException('the user is not in the conversation');
        }),
        catchError((err, caught) => {
          throw new BadRequestException('Conversation does not exists');
        }),
      );
  }
}
