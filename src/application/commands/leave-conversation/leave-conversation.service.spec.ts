import { Test, TestingModule } from '@nestjs/testing';
import { LeaveConversationService } from './leave-conversation.service';

describe('LeaveConversationService', () => {
  let service: LeaveConversationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LeaveConversationService],
    }).compile();

    service = module.get<LeaveConversationService>(LeaveConversationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
