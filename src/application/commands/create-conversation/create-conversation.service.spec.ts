import { Test, TestingModule } from '@nestjs/testing';
import { CreateConversationService } from './create-conversation.service';

describe('CreateConversationService', () => {
  let service: CreateConversationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CreateConversationService],
    }).compile();

    service = module.get<CreateConversationService>(CreateConversationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
