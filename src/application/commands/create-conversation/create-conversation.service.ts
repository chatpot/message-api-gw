import {Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';
import {
  ConversationsRemoteProxyService
} from 'src/application/remote-proxies/conversations-remote-proxy/conversations-remote-proxy.service';

@Injectable()
export class CreateConversationService {
  constructor(
      private readonly conversationsRemoteProxyService:
          ConversationsRemoteProxyService) {}
  execute(conversation: any, userId: string): Observable<any> {
    return this.conversationsRemoteProxyService.createConversation({
      ...conversation,
      members: conversation.members.includes(userId) ?
          [...conversation.members] :
          [...conversation.members, userId]
    });
  }
}
