import { Test, TestingModule } from '@nestjs/testing';
import { RefreshUserService } from './refresh-user.service';

describe('RefreshUserService', () => {
  let service: RefreshUserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RefreshUserService],
    }).compile();

    service = module.get<RefreshUserService>(RefreshUserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
