import { Injectable } from '@nestjs/common';
import { Observable, of } from 'rxjs';
import { UsersRemoteProxyService } from 'src/application/remote-proxies/users-remote-proxy/users-remote-proxy.service';
import { flatMap, catchError } from 'rxjs/operators';

export interface RefreshUserCommand {
  id: string;
  displayName: string;
  image: string;
}

@Injectable()
export class RefreshUserService {
  constructor(
    private readonly usersRemoteProxyService: UsersRemoteProxyService,
  ) {}

  execute(command: RefreshUserCommand): Observable<any> {
    return this.usersRemoteProxyService.getUser(command.id).pipe(
      flatMap(user => {
        if (
          user.displayName === command.displayName &&
          user.image === command.image &&
          user.id === command.id
        ) {
          return of(user);
        }
        return this.usersRemoteProxyService.updateUser(command);
      }),
      catchError((err, caught) => {
        return this.usersRemoteProxyService.createUser(command);
      }),
    );
  }
}
