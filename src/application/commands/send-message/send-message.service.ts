import {
  Injectable,
  UnauthorizedException,
  BadRequestException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { ConversationsRemoteProxyService } from 'src/application/remote-proxies/conversations-remote-proxy/conversations-remote-proxy.service';
import { MessagesRemoteProxyService } from 'src/application/remote-proxies/messages-remote-proxy/messages-remote-proxy.service';
import { flatMap } from 'rxjs/operators';

@Injectable()
export class SendMessageService {
  constructor(
    private readonly conversationsRemoteProxyService: ConversationsRemoteProxyService,
    private readonly messagesRemoteProxyService: MessagesRemoteProxyService,
  ) {}
  execute(userId: string, message: any): Observable<any> {
    if (message.senderId != userId)
      throw new BadRequestException('user id and sender id does not match');

    return this.conversationsRemoteProxyService
      .getConversation(message.conversationId)
      .pipe(
        flatMap(conversation => {
          if (conversation.members.includes(userId)) {
            return this.messagesRemoteProxyService.createMessage(message);
          }
          throw new UnauthorizedException(
            'user is not member of the conversation',
          );
        }),
      );
  }
}
