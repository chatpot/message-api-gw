import {Injectable, HttpService} from '@nestjs/common';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ConfigService} from '@nestjs/config';

@Injectable()
export class ConversationsRemoteProxyService {
  private readonly conversationEndpoint: string;

  constructor(
      private readonly httpService: HttpService,
      private readonly configService: ConfigService) {
    this.conversationEndpoint = this.configService.get("conversationEndpoint");
  }

  getConversation(conversationId: string): Observable<any> {
    return this.httpService.get(`${this.conversationEndpoint}${conversationId}`)
        .pipe(map(response => (response.data)));
  }

  createConversation(conversation: any): Observable<any> {
    return this.httpService.post(this.conversationEndpoint, conversation)
        .pipe(map(response => (response.data)));
  }

  getConversationsForUser(userId: string): Observable<any> {
    return this.httpService
        .get(`${this.conversationEndpoint}?memberId=${userId}`)
        .pipe(map(response => (response.data)));
  }

  updateConversation(conversation: any) {
    return this.httpService
        .put(`${this.conversationEndpoint}${conversation.id}`, conversation)
        .pipe(map(response => (response.data)));
  }
}
