import { Test, TestingModule } from '@nestjs/testing';
import { ConversationsRemoteProxyService } from './conversations-remote-proxy.service';

describe('ConversationsRemoteProxyService', () => {
  let service: ConversationsRemoteProxyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ConversationsRemoteProxyService],
    }).compile();

    service = module.get<ConversationsRemoteProxyService>(ConversationsRemoteProxyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
