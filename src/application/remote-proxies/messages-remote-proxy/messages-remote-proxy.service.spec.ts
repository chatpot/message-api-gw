import { Test, TestingModule } from '@nestjs/testing';
import { MessagesRemoteProxyService } from './messages-remote-proxy.service';

describe('MessagesRemoteProxyService', () => {
  let service: MessagesRemoteProxyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MessagesRemoteProxyService],
    }).compile();

    service = module.get<MessagesRemoteProxyService>(MessagesRemoteProxyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
