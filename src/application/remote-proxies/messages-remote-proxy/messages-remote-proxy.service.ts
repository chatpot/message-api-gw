import {Injectable, HttpService} from '@nestjs/common';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ConfigService} from '@nestjs/config';

@Injectable()
export class MessagesRemoteProxyService {
  private readonly messageEndpoint: string;

  constructor(
      private readonly httpService: HttpService,
      private readonly configService: ConfigService) {
    this.messageEndpoint = this.configService.get("messageEndpoint");
  }

  getMessagesForConversationId(conversationId: string): Observable<any> {
    return this.httpService
        .get(`${this.messageEndpoint}?conversationId=${conversationId}`)
        .pipe(map(response => (response.data)));
  }

  createMessage(message: any): Observable<any> {
    return this.httpService.post(this.messageEndpoint, message)
        .pipe(map(response => (response.data)));
  }
}
