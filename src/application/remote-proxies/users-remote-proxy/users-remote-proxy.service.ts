import {Injectable, HttpService} from '@nestjs/common';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ConfigService} from '@nestjs/config';

@Injectable()
export class UsersRemoteProxyService {
  private readonly userEndpoint: string;

  constructor(
      private readonly httpService: HttpService,
      private readonly configService: ConfigService) {
    this.userEndpoint = this.configService.get("userEndpoint");
  }

  deleteUser(id: string): Observable<any> {
    return this.httpService.delete(`${this.userEndpoint}${id}`)
        .pipe(map(response => { return response.data; }));
  }

  getUsers(): Observable<any[]> {
    return this.httpService.get(this.userEndpoint).pipe(map(response => {
                                                          return response.data;
                                                        }), );
  }

  getUser(id: string): Observable<any> {
    return this.httpService.get(`${this.userEndpoint}${id}`)
        .pipe(map(response => { return response.data; }), );
  }

  updateUser(command: any): Observable<any> {
    return this.httpService.put(`${this.userEndpoint}${command.id}`, command)
        .pipe(map(response => (response.data)));
  }

  createUser(command: any): Observable<any> {
    return this.httpService.post(this.userEndpoint, command)
        .pipe(map(response => (response.data)));
  }
}
