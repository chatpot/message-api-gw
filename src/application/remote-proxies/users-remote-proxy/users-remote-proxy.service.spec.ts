import { Test, TestingModule } from '@nestjs/testing';
import { UsersRemoteProxyService } from './users-remote-proxy.service';

describe('UsersRemoteProxyService', () => {
  let service: UsersRemoteProxyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersRemoteProxyService],
    }).compile();

    service = module.get<UsersRemoteProxyService>(UsersRemoteProxyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
