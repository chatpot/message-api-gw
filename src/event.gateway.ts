import {
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';

import { JwtService } from './auth/jwt/jwt.service';
import { SubscriberService } from './middleware/subscriber/subscriber.service';

@WebSocketGateway({ namespace: 'events' })
export class EventGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server: Server;

  private connectedSockets: Array<Socket> = new Array<Socket>();

  constructor(
    private jwtService: JwtService,
    private subscriberService: SubscriberService,
  ) {
    this.subscriberService.subscribe('to-front-gw', '#', message => {
      this.processMessage(message);
    });
  }

  processMessage(message): void {
    var routingKey = message.fields.routingKey;
    var content = message.content.toString();
    this.connectedSockets.forEach(socket => {
      socket.emit(routingKey, JSON.parse(content));
    });
  }

  afterInit(server: Server) {}

  handleDisconnect(client: Socket) {
    this.removeClient(client);
  }

  handleConnection(client: Socket, ...args: any[]) {}

  @SubscribeMessage('authenticate')
  handleAuthenticateEvent(client: Socket, token: string): void {
    this.jwtService
      .validate(token)
      .then(_ => {
        this.addClient(client);
      })
      .catch(_ => {
        this.removeClient(client);
      });
  }

  @SubscribeMessage('logout')
  handleLogoutEvent(client: Socket, token: string): void {
    this.removeClient(client);
  }

  private removeClient(client: Socket) {
    if (this.connectedSockets.find(socket => socket.id === client.id)) {
      this.connectedSockets = this.connectedSockets.filter(
        socket => socket.id !== client.id,
      );
    }
  }

  private addClient(client: Socket) {
    if (!this.connectedSockets.find(socket => socket.id === client.id)) {
      this.connectedSockets = [...this.connectedSockets, client];
    }
  }
}
