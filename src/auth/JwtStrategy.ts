import { Strategy } from 'passport-custom';

export class JwtStrategy extends Strategy {
  constructor(private readonly config: any) {
    super((req, done) => {
      return config.validator
        .validate(config.tokenProvider(req))
        .then((user: any) => {
          done(null, user);
        })
        .catch((reason: any) => {
          done(null, null);
        });
    });
  }
  name: string = 'custom';
}
