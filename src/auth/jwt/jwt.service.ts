import { Injectable, HttpService } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class JwtService {
  private keys: any;

  constructor(private httpService: HttpService) {
    this.httpService
      .get(
        'https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com',
      )
      .subscribe(response => {
        this.keys = response.data;
      });
  }

  validate(token: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      try {
        var decoded = jwt.decode(token, { complete: true });
        resolve(jwt.verify(token, this.keys[decoded.header.kid]));
      } catch (e) {
        reject(e);
      }
    });
  }
}
