import { Module, HttpModule } from '@nestjs/common';
import { CustomStrategy } from './CustomStrategy';
import { JwtService } from './jwt/jwt.service';
import { JwtStrategy } from './JwtStrategy';

@Module({
  imports: [HttpModule],
  providers: [CustomStrategy, JwtService, JwtStrategy],
  exports: [JwtService],
})
export class AuthModule {}
