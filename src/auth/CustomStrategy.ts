import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { JwtStrategy } from './JwtStrategy';
import { JwtService } from './jwt/jwt.service';
import { ExtractJwt } from 'passport-jwt';

@Injectable()
export class CustomStrategy extends PassportStrategy(JwtStrategy) {
  constructor(private readonly firebaseService: JwtService) {
    super({
      validator: firebaseService,
      tokenProvider: ExtractJwt.fromAuthHeaderAsBearerToken(),
    });
  }
}
